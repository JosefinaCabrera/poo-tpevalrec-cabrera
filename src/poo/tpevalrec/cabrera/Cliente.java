/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.tpevalrec.cabrera;

/**
 *
 * @author jocab
 */
public class Cliente {
    String dni;
    String ayn;
    String direccion;

    public Cliente() {
    }

    public Cliente(String dni, String ayn, String direccion) {
        this.dni = dni;
        this.ayn = ayn;
        this.direccion = direccion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
