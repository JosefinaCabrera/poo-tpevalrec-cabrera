/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.tpevalrec.cabrera;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jocab
 */
public class ListaEmpresa {
    Scanner sn = new Scanner(System.in);
    Empresa empresa = new Empresa();
    ArrayList<Empresa> listaempresas = new ArrayList();
    Cliente cliente = new Cliente();
    Empleado empleado = new Empleado();

    public void AgregarEmpresa() {
        String op;
        do {
            System.out.print("Ingrese un nombre para la Empresa: ");
            sn = new Scanner(System.in);
            String nom = sn.nextLine();
            empresa = new Empresa();
            empresa.setNomEmpresa(nom);
            listaempresas.add(empresa);
            System.out.print("Segir: s / n : ");
            op = sn.next();
        } while (op.equalsIgnoreCase("s"));
    }

    public void ListarEmpresa() {
        if (listaempresas.isEmpty()) {
            System.out.println("Esta Vacia la Lista");
        } else {
            System.out.println("LISTA DE EMPRESAS");
            for (int i = 0; i < listaempresas.size(); i++) {
                System.out.println("-" + listaempresas.get(i).getNomEmpresa());
            }
        }
    }

    public void AgregarClienteAEmp() {
        try {
            if (listaempresas.isEmpty()) {
                System.out.println("Esta Vacia la Lista");
            } else {
                System.out.println("LISTA DE EMPRESAS");
                for (int i = 0; i < listaempresas.size(); i++) {
                    System.out.println("-" + i + "- " + listaempresas.get(i).getNomEmpresa());
                }
                int n;
                do {
                    System.out.print("Eliga N° de Empresa: ");
                    n = sn.nextInt();
                } while (n > listaempresas.size());

                String op1;
                do {
                    System.out.println("Ingrese Datos del Cliente");
                    System.out.print("Dni: ");
                    String dni = sn.next();
                    System.out.print("AyN: ");
                    sn = new Scanner(System.in);
                    String ayn = sn.nextLine();
                    System.out.print("Direccion: ");
                    sn = new Scanner(System.in);
                    String direc = sn.nextLine();
                    cliente = new Cliente(dni, ayn, direc);
                    listaempresas.get(n).clientes.add(cliente);
                    System.out.print("Segir: s / n : ");
                    op1 = sn.next();
                } while (op1.equalsIgnoreCase("s"));
            }
        } catch (Exception e) {
            System.out.println("Error");
            sn = new Scanner(System.in);
        }
    }

    public void AgregarEmpleadoAEmp() {
        try {
            if (listaempresas.isEmpty()) {
                System.out.println("Esta Vacia la Lista");
            } else {
                System.out.println("LISTA DE EMPRESAS");
                for (int i = 0; i < listaempresas.size(); i++) {
                    System.out.println("-" + i + "- " + listaempresas.get(i).getNomEmpresa());
                }
                int n;
                do {
                    System.out.print("Eliga N° de Empresa: ");
                    n = sn.nextInt();
                } while (n > listaempresas.size());

                String op1;
                do {
                    System.out.println("Ingrese Datos del Empleado");
                    int codigo;
                    boolean bandera = false, bandera1 = false;
                    do {
                        System.out.print("Codigo: ");
                        codigo = sn.nextInt();
                        for (int i = 0; i < listaempresas.get(n).empleados.size(); i++) {
                            if (listaempresas.get(n).empleados.get(i).getCodigo() == codigo) {
                                bandera1 = true;
                            }
                        }
                        if (bandera1 == true) {
                            System.out.println("El codigo ya existe");
                            bandera1 = false;
                            bandera = true;
                        } else {
                            bandera = false;
                        }

                    } while (bandera == true);
                    System.out.print("AyN: ");
                    sn = new Scanner(System.in);
                    String ayn = sn.nextLine();
                    System.out.print("Tipo: ");
                    String tipo = sn.next();
                    System.out.print("VenatasMes: ");
                    int vmes = sn.nextInt();
                    System.out.print("Hs Extras: ");
                    int hsextras = sn.nextInt();
                    empleado = new Empleado(codigo, ayn, tipo, vmes, hsextras);
                    listaempresas.get(n).empleados.add(empleado);
                    System.out.print("Segir: s / n : ");
                    op1 = sn.next();
                } while (op1.equalsIgnoreCase("s"));
            }
        } catch (Exception e) {
            System.out.println("Error");
            sn = new Scanner(System.in);
        }
    }

    public void MostrarEmpListaCliente() {
        try {
            if (listaempresas.isEmpty()) {
                System.out.println("Esta Vacia la Lista");
            } else {
                System.out.println("LISTA DE EMPRESAS");
                for (int i = 0; i < listaempresas.size(); i++) {
                    System.out.println("-" + i + "- " + listaempresas.get(i).getNomEmpresa());
                }
                int n;
                do {
                    System.out.print("Eliga N° de Empresa: ");
                    n = sn.nextInt();
                } while (n > listaempresas.size());

                System.out.println("EMPRESA");
                System.out.println("-" + listaempresas.get(n).getNomEmpresa());
                System.out.println("Lista de Clientes de una Empresa");
                for (int i = 0; i < listaempresas.get(n).clientes.size(); i++) {
                    System.out.println("-" + listaempresas.get(n).clientes.get(i).getDni()
                            + " \n" + listaempresas.get(n).clientes.get(i).getAyn()
                            + " \n" + listaempresas.get(n).clientes.get(i).getDireccion());
                }
            }
        } catch (Exception e) {
            System.out.println("Error");
            sn = new Scanner(System.in);
        }
    }

    public void MostrarEmpListaEmpleado() {
        try {
            if (listaempresas.isEmpty()) {
                System.out.println("Esta Vacia la Lista");
            } else {
                System.out.println("LISTA DE EMPRESAS");
                for (int i = 0; i < listaempresas.size(); i++) {
                    System.out.println("-" + i + "- " + listaempresas.get(i).getNomEmpresa());
                }
                int n;
                do {
                    System.out.print("Eliga N° de Empresa: ");
                    n = sn.nextInt();
                } while (n > listaempresas.size());

                System.out.println("EMPRESA");
                System.out.println("-" + listaempresas.get(n).getNomEmpresa());
                System.out.println("Lista de Empleados de una Empresa");
                for (int i = 0; i < listaempresas.get(n).empleados.size(); i++) {
                    int salario = empleado.calculaSalarioBruto(listaempresas.get(n).empleados.get(i).getTipo(),
                            listaempresas.get(n).empleados.get(i).getVentasmes(),
                            listaempresas.get(n).empleados.get(i).getHsextras());
                    System.out.println("-" + listaempresas.get(n).empleados.get(i).getCodigo()
                            + " \n" + listaempresas.get(n).empleados.get(i).getAyn()
                            + " \n" + listaempresas.get(n).empleados.get(i).getTipo()
                            + " $" + listaempresas.get(n).empleados.get(i).getVentasmes()
                            + " \n" + listaempresas.get(n).empleados.get(i).getHsextras() + " hs\n"
                            + "Salario:" + empleado.calculaSalarioNeto(salario));
                }
            }
        } catch (Exception e) {
            System.out.println("Error");
            sn = new Scanner(System.in);
        }
    }
}
