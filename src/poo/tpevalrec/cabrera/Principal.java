/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.tpevalrec.cabrera;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author jocab
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        ListaEmpresa listemp=new ListaEmpresa();

        while (!salir) {
            try {
                System.out.println("-------- MENU PRINCIPAL ---------");
                System.out.println("1. Ingresar dato de una empresa");
                System.out.println("2. Listar todas las empresas");
                System.out.println("3. Agregar cliente a empresa");
                System.out.println("4. Agregar empleado a empresa");
                System.out.println("5. Mostrar listado de clientes de una empresa con todos sus datos");
                System.out.println("6. Mostrar empleados de una empresa con todos sus datos junto con el salario bruto y neto");
                System.out.println("7. Salir");

                System.out.print("Escribe una de las opciones: ");
                opcion = sn.nextInt();

                switch (opcion) {
                    case 1:
                        listemp.AgregarEmpresa();
                        break;
                    case 2:
                        listemp.ListarEmpresa();
                        break;
                    case 3:
                        listemp.AgregarClienteAEmp();
                        break;
                    case 4:
                        listemp.AgregarEmpleadoAEmp();
                        break;
                    case 5:
                        listemp.MostrarEmpListaCliente();
                        break;
                    case 6:
                        listemp.MostrarEmpListaEmpleado();
                        break;
                    case 7:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 7");
                }
                //EXCEPCIONES
            } catch (InputMismatchException e) {
                System.out.println("Error, Ingrese un valor numerico");
                sn = new Scanner(System.in);
            }
        }

    }   
    
}
