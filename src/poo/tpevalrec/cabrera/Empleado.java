/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.tpevalrec.cabrera;

/**
 *
 * @author jocab
 */
public class Empleado {
    int codigo;
    String ayn;
    String tipo;
    int ventasmes;
    int hsextras;

    public Empleado() {
    }

    public Empleado(int codigo, String ayn, String tipo, int ventasmes, int hsextras) {
        this.codigo = codigo;
        this.ayn = ayn;
        this.tipo = tipo;
        this.ventasmes = ventasmes;
        this.hsextras = hsextras;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getVentasmes() {
        return ventasmes;
    }

    public void setVentasmes(int ventasmes) {
        this.ventasmes = ventasmes;
    }

    public int getHsextras() {
        return hsextras;
    }

    public void setHsextras(int hsextras) {
        this.hsextras = hsextras;
    }
    public int calculaSalarioBruto(String tipo, int ventasMes, int horasExtra) {
        if (tipo.equalsIgnoreCase("vendedor")) {
            int salariobase = 10000;
            int total = salariobase;
            if (ventasMes >= 15000) {
                int prima = 1500;
                int totalhsextra = 200 * horasExtra;
                total = salariobase + prima + totalhsextra;
            } else if (ventasMes >= 10000) {
                int prima = 1000;
                int totalhsextra = 200 * horasExtra;
                total = salariobase + prima + totalhsextra;
            }
            return total;
        } else if (tipo.equalsIgnoreCase("encargado")) {
            int salariobase = 15000;
            int totalhsextra = 200 * horasExtra;
            int total = salariobase+totalhsextra;
            if (ventasMes >= 15000) {
                int prima = 1500;                
                total = salariobase + prima + totalhsextra;
            } else if (ventasMes >= 10000) {
                int prima = 1000;
                total = salariobase + prima + totalhsextra;
            }
            return total;
        } else if(tipo!="vendedor" || tipo!="encargado"){
            int salariobase = 0;
            int totalhsextra = 200 * horasExtra;
            int total = salariobase+totalhsextra;
            if (ventasMes >= 15000) {
                int prima = 1500;                
                total = salariobase + prima + totalhsextra;
            } else if (ventasMes >= 10000) {
                int prima = 1000;
                total = salariobase + prima + totalhsextra;
            }
            return total;
        }else{
            return 0;
        }
    }

    public float calculaSalarioNeto(int salarioBruto) {
        float salarioNeto = 0;
        if (salarioBruto < 10000) {
            salarioNeto = salarioBruto;
        } else {
            if (salarioBruto >= 10000 && salarioBruto < 15000) {              
                //salarioNeto = salarioBruto - salarioBruto * 16 / 100; //linea ERROR Se tendria que sumar(+)
                salarioNeto = salarioBruto + salarioBruto * 16 / 100;//lines CORRECCION
            } else {
                if (salarioBruto >= 15000) {
                    salarioNeto = salarioBruto + salarioBruto * 0.18f;
                }
            }
        }
        if (salarioBruto < 0) {
            return 0;
        } else {
            return salarioNeto;
        }
    }
}
