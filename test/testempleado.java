/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;
import poo.tpevalrec.cabrera.Empleado;

/**
 *
 * @author jocab
 */
public class testempleado {
    
    Empleado empleado=new Empleado();
    public testempleado() {
    }

    @Test
    public void calSalarioNetoMenorA0(){
        System.out.println("Menor a 0");
        float prueba=empleado.calculaSalarioNeto(0);        
        float esperado=0;
        System.out.println("prueba: "+prueba);
        System.out.println("esperado: "+esperado);
        assertEquals(esperado, prueba, 4);
    }
    @Test
    public void calSalarioNetoMenorA10000(){
        System.out.println("Menor a 10000 : "+9000);
        float prueba=empleado.calculaSalarioNeto(9000);        
        float esperado=9000;
        System.out.println("prueba: "+prueba);
        System.out.println("esperado: "+esperado);
        assertEquals(esperado, prueba, 4);
    }

    @Test
    public void calSalarioNetoEntre10000y15000(){
        System.out.println("Entre 10000 y 15000 : "+11000);
        float prueba=empleado.calculaSalarioNeto(11000);        
        float esperado=12760;
        System.out.println("prueba: "+prueba);
        System.out.println("esperado: "+esperado);
        assertEquals(esperado, prueba, 4);
    }
    
    @Test
    public void calSalarioNetoMayorA15000(){
        System.out.println("Mayor a 15000 : "+16000);
        float prueba=empleado.calculaSalarioNeto(16000);        
        float esperado=18880;
        System.out.println("prueba: "+prueba);
        System.out.println("esperado: "+esperado);
        assertEquals(esperado, prueba, 4);
    }
    
}
